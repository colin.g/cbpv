# CBPV

This is an implementation for a Call-by-Push Value interpreter with a
typechecker. The typechecker is based on the Inferno library,
[Hindley-Milner elaboration in applicative style](http://gallium.inria.fr/~fpottier/publis/fpottier-elaboration.pdf)
by François Pottier


# Quick start
- to install
```sh
$ ./configure
$ make install
```

- to build it
```sh
$ ./configure
$ make
```

- to install emacs dev tools (merlin, tuareg, ...)
```sh
$ make dev-deps
```

- to test it
```sh
$ make check
```

- to run it
```sh
$ make run
```

- to launch an interactive development top-level:
```sh
$ make top
```

# Dependencies:
`dune` and `opam` for setup and build.
