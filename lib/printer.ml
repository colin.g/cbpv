(************************************************************************************)
(*  MIT License                                                                     *)
(*                                                                                  *)
(*  Copyright (c) 2022 Colin González                                               *)
(*                                                                                  *)
(*  Permission is hereby granted, free of charge, to any person obtaining a copy    *)
(*  of this software and associated documentation files (the "Software"), to deal   *)
(*  in the Software without restriction, including without limitation the rights    *)
(*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *)
(*  copies of the Software, and to permit persons to whom the Software is           *)
(*  furnished to do so, subject to the following conditions:                        *)
(*                                                                                  *)
(*  The above copyright notice and this permission notice shall be included in all  *)
(*  copies or substantial portions of the Software.                                 *)
(*                                                                                  *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *)
(*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *)
(*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *)
(*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *)
(*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *)
(*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *)
(*  SOFTWARE.                                                                       *)
(************************************************************************************)

open PPrint
open Syntax.Terms
open Syntax.Types

let rec print_type t =
  match t with
  | TyApp ((name, ar), args) ->
      let sep = string name in
      if ar = 0 then sep
      else prefix 1 1 sep @@ parens (separate_map sep print_type args)
  | TyVar x -> string x

let print_record print_doc bindings =
  let contents =
    match bindings with
    | [ (id, value) ] -> !^id ^^ colon ^^ print_doc value
    | bs ->
        separate_map (semi ^^ space)
          (fun (id, value) -> infix 2 1 equals !^id (print_doc value))
          bs
  in
  braces contents

let rec print_term_list print_term list =
  match list with
  | Some xs, _ ->
      let contents =
        match xs with
        | [ component ] -> print_term component
        | _ -> separate_map (semi ^^ space) print_term xs
      in
      surround 2 0 lbracket contents rbracket
  | _ -> assert false

and print_term : type a. a term -> PPrint.document = fun t -> print_term_fun t

and print_term_fun : type a. a term -> PPrint.document =
 fun t ->
  match t with
  | TRange (_, t) -> print_term_fun t
  | Fun (x, _, t) -> !^"fun " ^^ !^x ^^ !^" ->" ^^ jump 2 1 (print_term_fun t)
  | Rec (x, _, t) -> !^"rec " ^^ !^x ^^ !^" =" ^^ jump 2 1 (print_term_fun t)
  | Let (x, _, t, c) ->
      prefix 1 1 !^"let" !^x ^^ jump 1 1 equals
      ^^ jump 1 1 (print_term_fun t)
      ^^ jump 2 1 !^"in"
      ^^ jump 2 1 (print_term_fun c)
  | To (t, x, _, c) ->
      print_term_fun t ^^ !^" to" ^^ jump 2 1 !^x ^^ jump 2 1 dot
      ^^ jump 2 1 (print_term_fun c)
  | _ -> print_term_app t

and print_term_app : type a. a term -> PPrint.document =
 fun t ->
  match t with
  | App (t1, t2) -> prefix 2 1 (print_term_atom t1) (print_term t2)
  | Force t -> prefix 1 1 !^"force" (print_term_atom t)
  | Thunk t -> prefix 1 1 !^"thunk" (print_term_atom t)
  | Return t -> prefix 1 1 !^"return" (print_term_atom t)
  | IfThenElse (c, x, y) ->
      prefix 1 1 !^"if" (print_term_atom c)
      ^^ jump 0 1 !^"then"
      ^^ jump 1 1 (print_term_atom x)
      ^^ jump 0 1 !^"else"
      ^^ jump 1 1 (print_term_atom y)
  | Prim t -> print_term_prim t
  | _ -> print_term_atom t

and print_term_atom : type a. a term -> PPrint.document =
 fun t ->
  match t with
  | Var x -> !^x
  | Lit c -> print_term_constant c
  | _ -> group (parens (print_term t))

and print_term_constant c =
  match c with Unit -> parens empty | Int i -> OCaml.int i

and print_term_stream print_component components =
  separate_map (twice colon) print_component components

and print_term_infix_app : type a b. document -> a term -> b term -> document =
 fun op t1 t2 -> infix 1 1 op (print_term_atom t1) (print_term_atom t2)

and print_term_prim : type a. a prim -> document =
 fun p ->
  match p with
  | Plus (x, y) -> print_term_infix_app plus x y
  | Minus (x, y) -> print_term_infix_app minus x y
  | Mult (x, y) -> print_term_infix_app star x y
  | Div (x, y) -> print_term_infix_app slash x y
  | Fst t -> prefix 1 1 !^"fst" (print_term_atom t)
  | Snd t -> prefix 1 1 !^"snd" (print_term_atom t)
  | CompIZ t -> prefix 1 1 (print_term_atom t) !^"=? 0"

let rec print_terminal : type a. a terminal -> document =
 fun t ->
  match t with
  | VUnit -> parens empty
  | VInt i -> OCaml.int i
  | VBool i -> OCaml.bool i
  | VPair (a, b) ->
      parens (separate_map (comma ^^ space) print_terminal [ a; b ])
  | VThunk _ -> angles !^"thunk"
  | VForce t -> prefix 2 1 bang (print_terminal t)
  | VFun (_, _, _) -> angles !^"fun"
  (* | VRec _ -> angles !^"…" *)
  | VReturn t -> prefix 2 1 !^"return" (print_terminal t)

let print_env e =
  separate_map hardline
    (fun (l, v) -> prefix 2 1 (enclose !^l equals colon) @@ print_terminal !v)
    e

let string_of_env e = Output.document_to_string print_env e

let fmt_of_term : type a. Format.formatter -> a term -> unit =
 fun fmt t -> Output.document_to_format print_term fmt t

let fmt_of_terminal : type a. Format.formatter -> a terminal -> unit =
 fun fmt t -> Output.document_to_format print_terminal fmt t

let fmt_of_type : Format.formatter -> htyp -> unit =
 fun fmt t -> Output.document_to_format print_type fmt t

let print_termln : type a. a term -> unit =
 fun t -> Format.printf "%a\n" fmt_of_term t

let print_terminalln : type a. a terminal -> unit =
 fun t -> Format.printf "%a\n" fmt_of_terminal t

let string_of_term : type a. a term -> string =
 fun t -> Output.document_to_string print_term t

let string_of_terminal : type a. a terminal -> string =
 fun t -> Output.document_to_string print_terminal t

let string_of_type : htyp -> string =
 fun t -> Output.document_to_string print_type t
