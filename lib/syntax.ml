(************************************************************************************)
(*  MIT License                                                                     *)
(*                                                                                  *)
(*  Copyright (c) 2022 Colin González                                               *)
(*                                                                                  *)
(*  Permission is hereby granted, free of charge, to any person obtaining a copy    *)
(*  of this software and associated documentation files (the "Software"), to deal   *)
(*  in the Software without restriction, including without limitation the rights    *)
(*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *)
(*  copies of the Software, and to permit persons to whom the Software is           *)
(*  furnished to do so, subject to the following conditions:                        *)
(*                                                                                  *)
(*  The above copyright notice and this permission notice shall be included in all  *)
(*  copies or substantial portions of the Software.                                 *)
(*                                                                                  *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *)
(*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *)
(*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *)
(*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *)
(*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *)
(*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *)
(*  SOFTWARE.                                                                       *)
(************************************************************************************)

type range = Lexing.position * Lexing.position

module Types = struct
  type tycomp = TyComp
  and tyval = TyVal

  type tykon = string * int
  and htyp = TyVar of string | TyApp of tykon * htyp list
end

module Terms = struct
  open Types

  type _ term =
    | TRange : range * 'a term -> 'a term
    | Lit : lit -> tyval term
    | Var : string -> tyval term
    | Fun : string * htyp * tycomp term -> tycomp term
    | App : tycomp term * tyval term -> tycomp term
    | Pair : tyval term * tyval term -> tyval term
    | Let : string * htyp * tyval term * tycomp term -> tycomp term
    | Rec : string * htyp * tycomp term -> tycomp term
    | To : tycomp term * string * htyp * tycomp term -> tycomp term
    | Force : tyval term -> tycomp term
    | Thunk : tycomp term -> tyval term
    | Return : tyval term -> tycomp term
    | IfThenElse : tyval term * tycomp term * tycomp term -> tycomp term
    | Prim : 'a prim -> 'a term

  and lit = Unit | Int of int

  and _ prim =
    | Fst : tyval term -> tycomp prim
    | Snd : tyval term -> tycomp prim
    | Plus : tyval term * tyval term -> tycomp prim
    | Minus : tyval term * tyval term -> tycomp prim
    | Mult : tyval term * tyval term -> tycomp prim
    | Div : tyval term * tyval term -> tycomp prim
    | CompIZ : tyval term -> tycomp prim

  and _ terminal =
    | VUnit : tyval terminal
    | VInt : int -> tyval terminal
    | VBool : bool -> tyval terminal
    | VPair : tyval terminal * tyval terminal -> tyval terminal
    | VThunk : tycomp term * env -> tyval terminal
    | VForce : tyval terminal -> tycomp terminal
    | VFun : string * tycomp term * env -> tycomp terminal
    | VReturn : tyval terminal -> tycomp terminal

  and env = (string * tyval terminal ref) list
  and context = { env : env }

  let vint x = VInt x
  let vbool x = VBool x
  let vunit = VUnit
  let vpair x y = VPair (x, y)
  let ty_return ty = TyApp (("F", 1), [ ty ])
  let ty_thunk ty = TyApp (("U", 1), [ ty ])
  let ty_arrow ty1 ty2 = TyApp (("->", 2), [ ty1; ty2 ])
  let ty_unit = TyApp (("unit", 0), [])
  let ty_int = TyApp (("int", 0), [])
  let ty_bool = TyApp (("bool", 0), [])
  let ty_prod ty1 ty2 = TyApp (("*", 2), [ ty1; ty2 ])
  let tfst x = Prim (Fst x)
  let tsnd x = Prim (Snd x)
  let tlet x ty t u = Let (x, ty, t, u)
  let fix x ty t = Rec (x, ty, t)
  let ( =: ) t (x, ty) u = To (t, x, ty, u)
  let ( |>! ) u t = App (t, u)
  let ( @@! ) t u = App (t, u)
  let tthunk t = Thunk t
  let tfun x ty t = Fun (x, ty, t)
  let tforce t = Force t
  let ( !! ) = tforce
  let treturn t = Return t
  let t x = Var x
  let ( +! ) x t = Prim (Plus (x, t))
  let ( *! ) x t = Prim (Mult (x, t))
  let ( -! ) x t = Prim (Minus (x, t))
  let tunit = Lit Unit
  let tint x = Lit (Int x)
  let tprod x y = Pair (x, y)
  let tif c x y = IfThenElse (c, x, y)
  let tfunn xs t = List.fold_right (fun (x, ty) t -> tfun x ty t) xs t
end

exception ParsingError of Position.t
