(************************************************************************************)
(*  MIT License                                                                     *)
(*                                                                                  *)
(*  Copyright (c) 2022 Colin González                                               *)
(*                                                                                  *)
(*  Permission is hereby granted, free of charge, to any person obtaining a copy    *)
(*  of this software and associated documentation files (the "Software"), to deal   *)
(*  in the Software without restriction, including without limitation the rights    *)
(*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *)
(*  copies of the Software, and to permit persons to whom the Software is           *)
(*  furnished to do so, subject to the following conditions:                        *)
(*                                                                                  *)
(*  The above copyright notice and this permission notice shall be included in all  *)
(*  copies or substantial portions of the Software.                                 *)
(*                                                                                  *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *)
(*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *)
(*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *)
(*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *)
(*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *)
(*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *)
(*  SOFTWARE.                                                                       *)
(************************************************************************************)

open Lexing

type lexing_position = Lexing.position

type t = { start_p : lexing_position; end_p : lexing_position }
[@@deriving sexp]

type position = t

type 'a located = { value : 'a; position : t } [@@deriving sexp]

let value { value; _ } = value

let position { position; _ } = position

let destruct p = (p.value, p.position)

let located f x = f (value x)

let _with_pos p v = { value = v; position = p }

let with_pos p1 p2 v = _with_pos { start_p = p1; end_p = p2 } v

let map f v = { value = f v.value; position = v.position }

let iter f { value; _ } = f value

let mapd f v =
  let w1, w2 = f v.value in
  let pos = v.position in
  ({ value = w1; position = pos }, { value = w2; position = pos })

let dummy = { start_p = Lexing.dummy_pos; end_p = Lexing.dummy_pos }

let unknown_pos v = { value = v; position = dummy }

let start_of_position { start_p; _ } = start_p

let end_of_position { end_p; _ } = end_p

let filename_of_position p = p.start_p.Lexing.pos_fname

let line { pos_lnum; _ } = pos_lnum

let column { pos_cnum; pos_bol; _ } = pos_cnum - pos_bol

let characters p1 p2 = (column p1, p2.pos_cnum - p1.pos_bol)

let join x1 x2 =
  {
    start_p = (if x1 = dummy then x2.start_p else x1.start_p);
    end_p = (if x2 = dummy then x1.end_p else x2.end_p);
  }

let lex_join x1 x2 = { start_p = x1; end_p = x2 }

let string_of_lex_pos p =
  let c = p.pos_cnum - p.pos_bol in
  Printf.sprintf "%d:%d" p.pos_lnum c

let string_of_pos p =
  let filename = filename_of_position p in
  let l = line p.start_p in
  let c1, c2 = characters p.start_p p.end_p in
  if filename = "" then Printf.sprintf "Line %d, characters %d-%d" l c1 c2
  else Printf.sprintf "File \"%s\", line %d, characters %d-%d" filename l c1 c2

let pos_of_undex = function None -> dummy | Some x -> x

let cpos lexbuf =
  { start_p = Lexing.lexeme_start_p lexbuf; end_p = Lexing.lexeme_end_p lexbuf }

let with_cpos lexbuf v = _with_pos (cpos lexbuf) v

let string_of_cpos lexbuf = string_of_pos (cpos lexbuf)
