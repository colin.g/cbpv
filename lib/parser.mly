%{ (* Emacs, open this with -*- tuareg -*- *)
   open Syntax.Terms

   type binop =
     | PPlus
     | PMinus
     | PMult
     | PDiv

   let make_binop op lhs rhs =
     match op with
     | PPlus  ->
        Plus (lhs, rhs)
     | PMinus ->
        Minus (lhs, rhs)
     | PMult ->
        Mult (lhs, rhs)
     | PDiv ->
        Div (lhs, rhs)


%}


%token EOF
%token LET
%token REC
%token THUNK
%token FORCE
%token RETURN
%token FUN
%token FST
%token SND
%token IF
%token THEN
%token ELSE
%token ARROW
%token COMMA
%token LPAREN
%token RPAREN
%token PLUS
%token MINUS
%token MULT
%token DIV
%token UNIT
%token <string> ID
%token <int> INT
%token IN TO
%token EQUAL
%token COLON
%token COMPIZ
%token TY_INT
%token TY_BOOL
%token TY_UNIT
%token TY_THUNK
%token TY_RETURN
%token EXIT
%start <Syntax.Types.tycomp Syntax.Terms.term> program
%%
program:
  | c=located(computation) EOF { c }
  | EXIT { exit 0 }
  | error
    {
      raise (Syntax.ParsingError (Position.lex_join $startpos $endpos))
    }

computation:
  |  bc=located(simple_computation) TO b=binding IN c=located(computation)
    { let (x, ty) = b in To (bc, x, ty, c) }
  | REC b=binding EQUAL c=located(computation)
    { let (x,ty) = b in   Rec(x,ty,c) }
  | LET b=binding EQUAL v=located(value) IN c=located(computation)
    { let (x, ty) = b in Let(x,ty,v,c) }
  | t=func { t }
  | IF c=located(value) THEN bt=located(computation) ELSE bf=located(computation)
    { IfThenElse(c,bt,bf) }
   | lhs=located(value) op=bin_op rhs=located(value)
     { Prim (make_binop op lhs rhs) }
  | c=simple_computation { c }

simple_computation:
  | c=located(simple_computation) v=located(atomic_value)
    { App (c, v) }
  | FORCE v=located(atomic_value)
    { Force v }
  | RETURN v=located(atomic_value)
    { Return v }
  | FST v=located(atomic_value)
    { Prim (Fst v) }
  | SND v=located(atomic_value)
    { Prim (Snd v) }
  | COMPIZ v=located(atomic_value)
    { Prim (CompIZ v) }
  | LPAREN c=computation RPAREN { c }

value:
  | LPAREN lhs=located(value) COMMA rhs=located(value) RPAREN
    { Pair (lhs, rhs) }
  | v=simple_value { v }

simple_value:
  | THUNK c=located(simple_computation)
    { Thunk c }
  | v=atomic_value { v }

atomic_value:
  | v=located(literal_or_id)
  | LPAREN v=value RPAREN { v }

literal_or_id:
  | i=ID { Var i }
  | UNIT { Lit Unit }
  | i=INT { Lit (Int i)}

%inline func:
  | FUN b=binding ARROW c=located(computation) { let (x,ty) = b in  Fun (x,ty,c) }

%inline bin_op:
  | PLUS   { PPlus   }
  | MINUS  { PMinus  }
  | MULT   { PMult  }
  | DIV    { PDiv    }

%inline located(X): x=X {
  TRange (($startpos, $endpos), x )
}

%inline binding: LPAREN x=ID COLON ty=typ RPAREN
{
  (x, ty)
}

typ: x=type_constant
{
  x
}
| lhs=type_constant ARROW rhs=typ
{
  ty_arrow lhs rhs
}
| lhs=type_constant MULT rhs=typ
{
  ty_prod lhs rhs
}
| TY_THUNK  t=typ
{
  ty_thunk t
}
| TY_RETURN  t=typ
{
  ty_return t
}

type_constant:
TY_INT
{
 ty_int
}
| TY_BOOL
{
 ty_bool
}
| TY_UNIT
{
 ty_unit
}
| LPAREN t=typ RPAREN
{
  t
}
