(************************************************************************************)
(*  MIT License                                                                     *)
(*                                                                                  *)
(*  Copyright (c) 2022 Colin González                                               *)
(*                                                                                  *)
(*  Permission is hereby granted, free of charge, to any person obtaining a copy    *)
(*  of this software and associated documentation files (the "Software"), to deal   *)
(*  in the Software without restriction, including without limitation the rights    *)
(*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *)
(*  copies of the Software, and to permit persons to whom the Software is           *)
(*  furnished to do so, subject to the following conditions:                        *)
(*                                                                                  *)
(*  The above copyright notice and this permission notice shall be included in all  *)
(*  copies or substantial portions of the Software.                                 *)
(*                                                                                  *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *)
(*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *)
(*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *)
(*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *)
(*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *)
(*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *)
(*  SOFTWARE.                                                                       *)
(************************************************************************************)

open Lexing

let open_in filename =
  let lexbuf = from_channel (open_in filename) in
  lexbuf.lex_curr_p <-
    { pos_fname = filename; pos_lnum = 1; pos_bol = 0; pos_cnum = 0 };
  lexbuf

let newline lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum }

let prompt = ref ""
let set_prompt = ( := ) prompt

let print_prompt () =
  output_string stdout !prompt;
  flush stdout

let input_char =
  let display_prompt = ref true in
  let ask stdin =
    if !display_prompt then (
      display_prompt := false;
      print_prompt ());
    let c = input_char stdin in
    if c = '\n' then display_prompt := true;
    String.make 1 c
  in
  ask

let read () =
  let b = Buffer.create 13 in
  let rec read prev =
    let c = input_char stdin in
    if c = "\n" then
      if prev <> "\\" then (
        Buffer.add_string b prev;
        let b = Buffer.contents b in
        if b = "" then read "" else b)
      else (
        set_prompt "....> ";
        Buffer.add_string b "\n";
        read c)
    else (
      Buffer.add_string b prev;
      read c)
  in
  read ""
