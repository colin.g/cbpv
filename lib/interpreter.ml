(************************************************************************************)
(*  MIT License                                                                     *)
(*                                                                                  *)
(*  Copyright (c) 2022 Colin González                                               *)
(*                                                                                  *)
(*  Permission is hereby granted, free of charge, to any person obtaining a copy    *)
(*  of this software and associated documentation files (the "Software"), to deal   *)
(*  in the Software without restriction, including without limitation the rights    *)
(*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *)
(*  copies of the Software, and to permit persons to whom the Software is           *)
(*  furnished to do so, subject to the following conditions:                        *)
(*                                                                                  *)
(*  The above copyright notice and this permission notice shall be included in all  *)
(*  copies or substantial portions of the Software.                                 *)
(*                                                                                  *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *)
(*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *)
(*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *)
(*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *)
(*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *)
(*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *)
(*  SOFTWARE.                                                                       *)
(************************************************************************************)

let process_term ~lexer_init ~input =
  Parser.program Lexer.token (lexer_init input)

let parse =
  let parse lexer_init input = process_term ~lexer_init ~input in
  let parse_string = parse Lexing.from_string in
  parse_string

let parse_file path =
  let parse lexer_init input = process_term ~lexer_init ~input in
  try
    let parse_string = parse Input.open_in path in
    parse_string
  with
  | Sys_error msg -> failwith (Printf.sprintf "%s during parsing." msg)
  | Lexer.LexingError lexbuf ->
      let pos =
        Position.{ start_p = lexbuf.lex_start_p; end_p = lexbuf.lex_curr_p }
      in
      failwith
        (Printf.sprintf "%s:\n Syntax error, unknown token."
           (Position.string_of_pos pos))
  | Syntax.ParsingError pos ->
      failwith
        (Printf.sprintf "%s:\n Syntax error." (Position.string_of_pos pos))

let read = Input.read
let check t = Typechecker.check t

let eval :
    Syntax.Types.tycomp Syntax.Terms.term ->
    Syntax.Types.tycomp Syntax.Terms.terminal =
 fun t -> Semantics.eval t { env = [] }

let print (ty, t) =
  let ty_s = Printer.string_of_type ty in
  let t_s = Printer.string_of_terminal t in
  Printf.printf "_ : %s = %s\n" ty_s t_s

let eval_interactive s =
  let t = parse s in
  let ty, t = check t in
  (ty, eval t)

let eval_file f =
  let t = parse_file f in
  let ty, t = check t in
  print (ty, eval t)

let rec read_eval_print () =
  Input.set_prompt "cbpv>";
  try
    read () |> eval_interactive |> print;
    Format.print_flush ();
    read_eval_print ()
  with
  | Sys_error msg ->
      Printf.eprintf "%s during parsing.\n" msg;
      flush_all ();
      read_eval_print ()
  | Lexer.LexingError lexbuf ->
      let pos =
        Position.{ start_p = lexbuf.lex_start_p; end_p = lexbuf.lex_curr_p }
      in
      Printf.eprintf "%s:\n Syntax error, unknown token.\n"
        (Position.string_of_pos pos);
      flush_all ();
      read_eval_print ()
  | Syntax.ParsingError pos ->
      Printf.eprintf "%s:\n Syntax error.\n" (Position.string_of_pos pos);
      flush_all ();
      read_eval_print ()
