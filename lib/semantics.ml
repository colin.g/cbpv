(************************************************************************************)
(*  MIT License                                                                     *)
(*                                                                                  *)
(*  Copyright (c) 2022 Colin González                                               *)
(*                                                                                  *)
(*  Permission is hereby granted, free of charge, to any person obtaining a copy    *)
(*  of this software and associated documentation files (the "Software"), to deal   *)
(*  in the Software without restriction, including without limitation the rights    *)
(*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *)
(*  copies of the Software, and to permit persons to whom the Software is           *)
(*  furnished to do so, subject to the following conditions:                        *)
(*                                                                                  *)
(*  The above copyright notice and this permission notice shall be included in all  *)
(*  copies or substantial portions of the Software.                                 *)
(*                                                                                  *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *)
(*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *)
(*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *)
(*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *)
(*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *)
(*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *)
(*  SOFTWARE.                                                                       *)
(************************************************************************************)

open Syntax
open Types
open Terms

type 'a eval_context = context -> 'a
type gterm = T : 'a term -> gterm

exception TypeError of gterm

let return (x : 'a) : 'a eval_context = fun _ -> x

let bind (m : 'a eval_context) (f : 'a -> 'b eval_context) : 'b eval_context =
 fun e -> f (m e) e

let ( let* ) = bind

let get x context =
  let env = context.env in
  try !(List.assoc x env)
  with Not_found -> failwith (Printf.sprintf "Unbound %s\n" x)

let put x v m context =
  let env = context.env in
  m { env = (x, ref v) :: env }

let print_env context =
  let env = context.env in
  Printf.printf "ENV : %s\n%!" (Printer.string_of_env env);
  return ()

let rec eval : type a. a term -> a terminal eval_context =
 fun t ->
  match (t : a term) with
  | TRange (_r, t) -> ( try eval' t with _ -> failwith "error")
  | Fun (x, _, t) -> fun context -> VFun (x, t, context.env)
  | Force t -> cforce t
  | Thunk t -> vthunk t
  | Var x -> get x
  | Lit g -> vlit g
  | Prim (Fst t) -> cproj true t
  | Prim (Snd t) -> cproj false t
  | Prim (Plus (x, y)) -> cbin_arith ( + ) x y
  | Prim (Mult (x, y)) -> cbin_arith ( * ) x y
  | Prim (Minus (x, y)) -> cbin_arith ( - ) x y
  | Prim (Div (x, y)) -> cbin_arith ( / ) x y
  | Prim (CompIZ t) -> comp_iz t
  | App (t, u) -> capp t u
  | Return t -> creturn t
  | To (t, x, _, u) -> cto t x u
  | Let (x, _, t, u) -> clet x t u
  | Rec (x, _, t) -> crec x t
  | Pair (x, y) ->
      let* f = eval x in
      let* s = eval y in
      return (vpair f s)
  | IfThenElse (c, x, y) -> cite c x y

and eval_debug : type a. a term -> a terminal eval_context =
 fun t ->
  Printf.printf "----------------------------------\n";
  let* _ = print_env in
  Printf.printf "EVAL TERM : %s%!\n" (Printer.string_of_term t);
  eval t

and eval' : type a. a term -> a terminal eval_context = fun t -> eval t

and vthunk t : tyval terminal eval_context =
 fun context -> VThunk (t, context.env)

and clet x t u : tycomp terminal eval_context =
  let* t' = eval' t in
  put x t' (eval' u)

and creturn t : tycomp terminal eval_context =
  let* u = eval' t in
  return (VReturn u)

and capp t u : tycomp terminal eval_context =
  let* v = eval' u in
  let* f = eval' t in
  match f with
  | VFun (x, t, env) -> return (eval' t { env = (x, ref v) :: env })
  | _ -> failwith "Ill typed application"

and cforce : tyval term -> tycomp terminal eval_context =
 fun t context ->
  match eval' t context with
  | VThunk (t, env) -> eval' t { env }
  | _ -> failwith "force not well typed"

and cto t x u : tycomp terminal eval_context =
  let* t' = eval' t in
  match t' with VReturn v -> put x v (eval' u) | _ -> failwith "Ill typed to"

and crec x t : tycomp terminal eval_context =
 fun context ->
  let env = context.env in
  let env = (x, ref VUnit) :: env in
  let rect = VThunk (t, env) in
  List.assoc x env := rect;
  eval' t { env }

and cite c x y : tycomp terminal eval_context =
  let* c' = eval' c in
  match c' with
  | VBool t -> if t then eval' x else eval' y
  | _ -> failwith "Ill typed condition in if-control statement"

and vlit t : tyval terminal eval_context =
  match t with Unit -> return vunit | Int i -> return (vint i)

and cproj i t : tycomp terminal eval_context =
  let* p = eval' t in
  match (i, p) with
  | true, VPair (x, _) -> return (VReturn x)
  | false, VPair (_, y) -> return (VReturn y)
  | _ -> failwith "Ill typed projection"

and comp_iz t : tycomp terminal eval_context =
  let* p = eval' t in
  match p with
  | VInt 0 -> return (VReturn (VBool true))
  | VInt _ -> return (VReturn (VBool false))
  | _ -> failwith "Ill typed projection"

and cbin_arith op x y : tycomp terminal eval_context =
  let* lhs = eval' x in
  let* rhs = eval' y in
  match (lhs, rhs) with
  | VInt x, VInt y -> return (VReturn (vint (op x y)))
  | _, _ -> failwith "arith not well typed"
