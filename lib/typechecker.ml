(************************************************************************************)
(*  MIT License                                                                     *)
(*                                                                                  *)
(*  Copyright (c) 2022 Colin González                                               *)
(*                                                                                  *)
(*  Permission is hereby granted, free of charge, to any person obtaining a copy    *)
(*  of this software and associated documentation files (the "Software"), to deal   *)
(*  in the Software without restriction, including without limitation the rights    *)
(*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *)
(*  copies of the Software, and to permit persons to whom the Software is           *)
(*  furnished to do so, subject to the following conditions:                        *)
(*                                                                                  *)
(*  The above copyright notice and this permission notice shall be included in all  *)
(*  copies or substantial portions of the Software.                                 *)
(*                                                                                  *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *)
(*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *)
(*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *)
(*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *)
(*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *)
(*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *)
(*  SOFTWARE.                                                                       *)
(************************************************************************************)

module CBPV = Syntax.Terms
open CBPV
open Syntax.Types

module S = struct
  type 'a structure = TyApp of tykon * 'a list

  let map f (TyApp (t, ts)) = TyApp (t, List.map f ts)
  let fold f (TyApp (_, ts)) accu = List.fold_right f ts accu
  let iter f (TyApp (_, ts)) = List.iter f ts

  exception Iter2

  let iter2 f (TyApp (t, ts)) (TyApp (u, us)) =
    if t = u then List.iter2 f ts us else raise Iter2
end

module O = struct
  type tyvar = int
  type 'a structure = 'a S.structure
  type ty = htyp

  let solver_tyvar n = n

  let name_of_tyvar =
    let tbl = Hashtbl.create 32 in
    fun x ->
      match Hashtbl.find_opt tbl x with
      | Some x' -> x'
      | None ->
          let n = Printf.sprintf "'a_%d" x in
          Hashtbl.add tbl x n;
          n

  let variable (x : tyvar) = TyVar (name_of_tyvar x)
  let structure (S.TyApp (t, ts)) = TyApp (t, ts)
  let mu _ _ = failwith "Rectypes unsupported"

  type scheme = tyvar list * ty
end

module Solver =
  Inferno.SolverHi.Make
    (struct
      include String

      type tevar = t
    end)
    (S)
    (O)

open Solver

let returnty ty : 'a S.structure = S.TyApp (("F", 1), [ ty ])
let thunkty ty : 'a S.structure = S.TyApp (("U", 1), [ ty ])
let arrowty ty1 ty2 : 'a S.structure = S.TyApp (("->", 2), [ ty1; ty2 ])
let unitty : 's S.structure = S.TyApp (("unit", 0), [])
let intty : 's S.structure = S.TyApp (("int", 0), [])
let boolty : 's S.structure = S.TyApp (("bool", 0), [])
let prodty n ty : 's S.structure = S.TyApp (("*", n), ty)

let rec convert_deep (ty : htyp) =
  let conv = convert_deep in
  let deeps t = DeepStructure t in
  match ty with
  | TyApp (c, ts) ->
      let ts = List.map conv ts in
      deeps (TyApp (c, ts))
  | TyVar _ -> failwith "Types should be monomorphic"

let convert ty =
  let deep_ty = convert_deep ty in
  build deep_ty

exception Poly of htyp

let rec is_var (ty : htyp) =
  match ty with
  | TyVar _ -> Some ty
  | TyApp (_, ts) ->
      List.fold_left
        (fun maybe_var ty -> match maybe_var with None -> is_var ty | t -> t)
        None ts

let is_monomorphic ty = match is_var ty with Some _ -> false | _ -> true

let rec hastype :
    type t. (string * htyp) list -> t term -> variable -> t term co =
 fun env term w ->
  match term with
  | CBPV.TRange (r, t) ->
      correlate r (hastype env t w) <$$> fun t -> CBPV.TRange (r, t)
  | CBPV.Var x -> (
      (* Γ, x: A, Γ' ⊢ᵛ x : A *)
      try
        let ty = List.assoc x env in
        convert ty (fun v -> w -- v) <$$> fun _ -> term
      with Not_found ->
        raise (Solver.Unbound ((Lexing.dummy_pos, Lexing.dummy_pos), x)))
  | CBPV.Return t ->
      (* Γ ⊢ⱽ V : A
          ——————————
          Γ ⊢ᶜ return V : F A *)
      exist_ (fun v -> hastype env t v ^^ (w --- returnty v)) <$$> fun _ -> term
  | CBPV.Thunk t ->
      (* Γ ⊢ᶜ M : Ḇ
          —————————————————–
          Γ ⊢ᵛ thunk M : U Ḇ *)
      exist_ (fun v -> (w --- thunkty v) ^^ hastype env t v) <$$> fun _ -> term
  | CBPV.Force t ->
      (* Γ ⊢ᵛ V : U Ḇ
         —————————————————–
         Γ ⊢ᶜ force V : Ḇ *)
      lift (hastype env) t (thunkty w) <$$> fun _ -> term
  | CBPV.Fun (x, ty, t) ->
      (* Γ, x : A ⊢ᶜ M : Ḇ
         —————————————————–—
         Γ ⊢ᶜ λ x. M : A → Ḇ *)
      convert ty (fun v1 ->
          exist_ (fun v2 ->
              (w --- arrowty v1 v2) ^^ def x v1 (hastype ((x, ty) :: env) t v2)))
      <$$> fun _ -> term
  | CBPV.App (t1, t2) ->
      (* Γ ⊢ᵛ V : A  Γ ⊢ᶜ M : A → Ḇ
         —————————————————–———————–
              Γ ⊢ᶜ M V : Ḇ *)
      exist_ (fun v -> lift (hastype env) t1 (arrowty v w) ^^ hastype env t2 v)
      <$$> fun _ -> term
  | CBPV.Rec (x, ty, t) ->
      (* Γ,x : U Ḇ ⊢ᶜ M : Ḇ
         —————————————————–
          Γ ⊢ᶜ μ x. M : Ḇ *)
      convert ty (fun v ->
          def x v (hastype ((x, ty) :: env) t w) ^^ (v --- thunkty w))
      <$$> fun _ -> term
  | CBPV.Pair (f, s) ->
      (* Γ ⊢ᵛ V : A  Γ,x : A ⊢ᵛ W : A'
          —————————————————–———————–———–
               Γ ⊢ᵛ (V, W) : A * A' *)
      exist_ (fun v1 ->
          exist_ (fun v2 ->
              hastype env f v1 ^^ hastype env s v2 ^^ (w --- prodty 2 [ v1; v2 ])))
      <$$> fun _ -> term
  | CBPV.Let (x, ty, t, u) ->
      (* Γ ⊢ᵛ V : A  Γ,x : A ⊢ᶜ M : Ḇ
         —————————————————–———————–———–
              Γ ⊢ᶜ let x = V in M : Ḇ *)
      convert ty (fun v ->
          hastype env t v ^& def x v (hastype ((x, ty) :: env) u w))
      <$$> fun _ -> term
  | CBPV.To (m, x, ty, n) ->
      (* Γ ⊢ᶜ M : F A  Γ,x : A ⊢ᶜ N : Ḇ
         —————————————————–———————–———–——
              Γ ⊢ᶜ M to x in N : Ḇ *)
      convert ty (fun v ->
          lift (hastype env) m (returnty v)
          ^^ def x v (hastype ((x, ty) :: env) n w))
      <$$> fun _ -> term
  | CBPV.IfThenElse (c, b1, b2) ->
      (* Γ⊢ᵛ V:bool  Γ ⊢ᶜ M : Ḇ  Γ ⊢ᶜ N : Ḇ
         —————————————————–———————–———–————–
           Γ ⊢ᶜ if V then M else N : Ḇ *)
      lift (hastype env) c boolty ^^ hastype env b1 w ^^ hastype env b2 w
      <$$> fun _ -> term
  | CBPV.Lit c ->
      (* Γ ⊢ᵛ c: A *)
      hastype_const c w
  | CBPV.Prim p -> hastype_prim env p w

and hastype_const c w : tyval term co =
  match c with
  | CBPV.Unit ->
      (* Γ ⊢ᵛ (): unit *)
      w --- unitty <$$> fun () -> CBPV.Lit c
  | CBPV.Int _ ->
      (* Γ ⊢ᵛ c: int *)
      w --- intty <$$> fun () -> CBPV.Lit c

and hastype_prim :
    type t. (string * htyp) list -> t prim -> variable -> t term co =
 fun env p w ->
  let prim p = CBPV.Prim p in
  match p with
  | Fst t ->
      (* Γ ⊢ᵛ t : A * A'
         ————————————–—–——
         Γ ⊢ᶜ fst t : F A *)
      exist_ (fun vf ->
          exist_ (fun vs ->
              lift (hastype env) t (prodty 2 [ vf; vs ]) ^^ (w --- returnty vf)))
      <$$> fun _ -> prim p
  | Snd t ->
      (* Γ ⊢ᵛ t : A * A'
         ————————————–—–—–
         Γ ⊢ᶜ fst t : F A' *)
      exist_ (fun vf ->
          exist_ (fun vs ->
              lift (hastype env) t (prodty 2 [ vf; vs ]) ^^ (w --- returnty vs)))
      <$$> fun _ -> prim p
  | Plus (lhs, rhs) | Minus (lhs, rhs) | Mult (lhs, rhs) | Div (lhs, rhs) ->
      (* Γ ⊢ᵛ t : int   Γ ⊢ᵛ u : int
         ————————————–—–—————————————
               Γ ⊢ᶜ t op u : F int *)
      exist_ (fun v ->
          lift (hastype env) lhs intty
          ^^ lift (hastype env) rhs intty
          ^^ (v --- intty)
          ^^ (w --- returnty v))
      <$$> fun () -> prim p
  | CompIZ t ->
      (* Γ ⊢ᵛ t : int
         ————————————–—–—————————————
         Γ ⊢ᶜ is_zero t: F bool *)
      exist_ (fun v ->
          lift (hastype env) t intty ^^ (v --- boolty) ^^ (w --- returnty v))
      <$$> fun () -> prim p

type range = Solver.range

exception Unbound = Solver.Unbound
exception Unify = Solver.Unify
exception Cycle = Solver.Cycle

open Printf

let is_dummy (pos1, pos2) = pos1 == Lexing.dummy_pos && pos2 == Lexing.dummy_pos

let print_range f ((pos1, pos2) as range) =
  if is_dummy range then fprintf f "At an unknown location:\n"
  else
    let file = pos1.pos_fname in
    let line = pos1.pos_lnum in
    let char1 = pos1.pos_cnum - pos1.pos_bol in
    let char2 = pos2.pos_cnum - pos1.pos_bol in
    (* yes, [pos1.pos_bol] *)
    fprintf f "File \"%s\", line %d, characters %d-%d:\n" file line char1 char2

let rec deep_print_ty t =
  match t with
  | TyVar t -> t
  | TyApp ((tid, 0), []) -> tid
  | TyApp ((tid, 1), [ ty ]) -> Printf.sprintf "%s (%s)" tid (deep_print_ty ty)
  | TyApp (("->", 2), [ t1; t2 ]) ->
      Printf.sprintf "%s -> %s" (deep_print_ty t1) (deep_print_ty t2)
  | TyApp ((id, _), tys) ->
      Printf.sprintf "%s (%s)" id
        (List.fold_left (fun a ty -> a ^ deep_print_ty ty ^ ", ") "" tys)

let explain e =
  match e with
  | Unbound (_, x) -> eprintf "The variable \"%s\" is unbound.\n" x
  | Unify (_, ty1, ty2) ->
      eprintf "The following types cannot be unified:\n%s\n%s\n"
        (deep_print_ty ty1) (deep_print_ty ty2)
  | Cycle (_, ty) ->
      eprintf "The following type is cyclic:\n%s\n" (deep_print_ty ty)
  | _ -> assert false

let translate : type t. t term -> htyp * t term =
 fun t -> solve false (let0 (exist (hastype [] t)) <$$> fun (_, term) -> term)

let check t =
  try translate t
  with Unify (r, _, _) as e ->
    printf "TYPE ERROR\n";
    print_range stderr r;
    explain e;
    exit 0

let check : type t. t term -> htyp * t term = fun t -> check t
