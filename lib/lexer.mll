{ (* Emacs, open this with -*- tuareg -*- *)
  open Parser

let enter_newline lexbuf =
  Input.newline lexbuf;
  lexbuf

exception LexingError of Lexing.lexbuf
}

let newline = ('\013' | "\013\010" | '\010')
let whitespace = [ '\032' '\009' ]

let alpha = ['a'-'z''A'-'Z']

let digit = ['0'-'9']

let sym  = ['_']

let ident = alpha (alpha | digit | sym)*
rule token = parse
| newline     { enter_newline lexbuf |> token }
| whitespace  { token lexbuf          }
| digit+ as d { INT (int_of_string d) }
| "="         { EQUAL                 }
| "let"       { LET                   }
| "fun"       { FUN                   }
| "rec"       { REC                   }
| "force"     { FORCE                 }
| "thunk"     { THUNK                 }
| "return"    { RETURN                }
| "if"        { IF                    }
| "then"      { THEN                  }
| "else"      { ELSE                  }
| "in"        { IN                    }
| "fst"       { FST                   }
| "snd"       { SND                   }
| "is_zero"   { COMPIZ                }
| "=:"        { TO                    }
| "->"        { ARROW                 }
| ","         { COMMA                 }
| "("         { LPAREN                }
| ")"         { RPAREN                }
| "+"         { PLUS                  }
| "-"         { MINUS                 }
| "*"         { MULT                  }
| "/"         { DIV                   }
| "()"        { UNIT                  }
| ":"         { COLON                 }
| "exit"      { EXIT                  }
| "int"       { TY_INT                }
| "unit"      { TY_UNIT               }
| "bool"      { TY_BOOL               }
| "U"         { TY_THUNK              }
| "F"         { TY_RETURN             }
| ident as id { ID id                 }
| eof         { EOF                   }
| _
      { raise (LexingError (lexbuf))}
